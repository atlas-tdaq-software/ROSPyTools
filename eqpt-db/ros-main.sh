#!/bin/bash
#title          :ros-main.sh
#description    :This script will retrieve serial no., MACaddresses and other data from ROS equipment
#author         :mkvist
#date           :20160420
#version        :0.5
#usage          :sh ros-main.sh [OPTION]... [FILE]...
#note           :The script will output two .csv-files
#====================================================================================================

#Declare booleans
declare -ir false=0 true=1

#All global variables should be prefixed with "G_"
G_OUTPUT=''

readonly G_ROS_DATA_SH='ros-data.sh'
readonly G_ROS_FILE='ROS_Glance'
readonly G_ROBINNP_FILE='RobinNP_Glance.csv'
#comma as a field separator
readonly G_FIELD_SEPARATOR=','

#Array pretending to be a Pythonic dictionary
declare -a G_ROS_HASH=(
    #KEY::VALUE - the KEY is the CSV header, and the VALUE is the trailer
    #For clarity, you must use :: as an alias for, to indicate the KEY/VALUE pairs
    "Equipment ID::"
    "Responsible::VANDELLI"
    "Location Details::"
    "Type ID::20TDAQ103"
    "Equipment Type::MiriQuid"
    "Logical Group ID::TDAQ_COMPUTER"
    "Logical Group::TDAQ Computer"
    "Manufacturer::MEGWARE"
    "Hardware Status::Good"
    "Installation Status::I"
    "Equipment Comment::N/A"
    "Serial Number::"
    "Budget Code::T575080"
    "Order Number::5668809"
    "PC Warranty Period::8/1/19"
    "Contact Address for Repair::MEGWARE COMPUTER GMBH, HPC & Cluster Business Unit Nordstrasse 19, 09247 CHEMNITZ-ROEHRSDORF"
    "Motherboard Serial Numbers::N/A"
    "Memory Module Serial Numbers::N/A"
    "CPU Serial Numbers::N/A"
    "MAC Address 1::"
    "MAC Address 2::"
    "Functional Name::"
    "Power Supply Chassis Serial Number::N/A"
    "Power Supply Module Serial Numbers::N/A"
    "IPMI Module MAC Address::"
    "IPMI Version::N/A"
    "Installation Date::02/2017")

declare -a G_ROBINNP_HASH=(
    "Equipment ID::"
    "Parent::"
    "Responsible::VANDELLI"
    "Type ID::20TDAQ104"
    "Equipment Type::RobinNP"
    "Logical Group ID::TDAQ_ROS_ROBIN_CARD"
    "Logical Group::TDAQ ROS ROBIN Card"
    "Manufacturer::HARPRO"
    "Hardware Status::Good"
    "Installation Status::I"
    "Equipment Comment::N/A"
    "Installation Date::02/2017"
    "Production Date::08/2014"
    "Schematic Version::N/A"
    "Gerber Version::N/A"
    "PCB Model Name::N/A"
    "PCB Identification::N/A"
    "BOM Version::N/A"
    "MAC Address::"
    "Functional Name::"
    "Assembler ID::"
    "Owning Institute::N/A"
    "OSC Replaced::N/A"
    "Test Date::N/A"
    "Hardware Serial Number::N/A"
    "Equipment Comment::N/A"
    "Slot Index::")

### MAIN function ###
function main
{
    declare -a errorlist
    declare -a inputfiles

    local tmp=
    local ros_file=
    local robinnp_file=
    local no_of_ros=0
    local progress_num=1
    local only_one_is_allowed=false
    local ros_equipment_id=$(HashLookup G_ROS_HASH "Equipment ID")
    local robinnp_equipment_id=$(HashLookup G_ROBINNP_HASH "Equipment ID")

    #Parse command line flags
    while getopts 'h' flag; do
	case "${flag}" in
	    h) UtilShowHelp; exit 0 ;;
	    #Unrecognized option - show help
	    \?) UtilShowHelp; exit 1;;
	esac
    done
    FileExists $G_ROS_DATA_SH

    #The only rule is that the file or files you are processing
    #have to come after all of the option switches.

    #This tells getopts to move on to the next argument
    shift $((OPTIND-1))

    #Test and check input files ...
    if [ $# -lt 1 ]; then
	echo "Missing input file(s)!"
	exit 1
    fi
    while [ $# -ne 0 ]; do
	FileExists $1
	inputfiles=("${inputfiles[@]}" $1)
	while read -r line || [[ -n "$line" ]]; do
	    if [ -z "$line" ]; then :
	    else [[ "$line" =~ ^#.*$ ]] && {
                    no_whitespace=$(echo -e "$line" | tr -d '[:space:]')
                    #convert $no_whitespace to uppercase
                    if [[ "${no_whitespace^^}" =~ "ROSEQUIPMENTID" ]]; then
                        ros_equipment_id=${no_whitespace##*=}
                        only_one_is_allowed=true

                    elif [[ "${no_whitespace^^}" =~ "ROBINNPEQUIPMENTID" ]]; then
                        robinnp_equipment_id=${no_whitespace##*=}
                        only_one_is_allowed=true; fi

                    continue; }
		((no_of_ros=no_of_ros+1))
	    fi
	done < $1
	shift
    done
    #Only one input file is allowed when the Equipment IDs
    #are read from input file.
    if [ "${#inputfiles[@]}" -gt 1 ]; then ((only_one_is_allowed)) && {
        echo "Only one input file is allowed in this mode"
        exit 1; }
    fi

    #Enter Equipment IDs
    while [ -z $ros_equipment_id ]; do echo -n "Enter ROS Equipment ID: "; read ros_equipment_id; break; done
    while [ -z $robinnp_equipment_id ]; do echo -n "Enter RobinNP Equipment ID: "; read robinnp_equipment_id; break; done

    #Check if valid IDs
    tmp=$(ROSResolveNextId $ros_equipment_id); if [ $? -ne 0 ]; then echo "Bad equipment ID $ros_equipment_id"; exit 1; fi
    tmp=$(ROSResolveNextId $robinnp_equipment_id); if [ $? -ne 0 ]; then echo "Bad equipment ID $robinnp_equipment_id"; exit 1; fi

    #Handle output files
    ros_file=$(FileExtensionIsCSV $G_ROS_FILE)
    ros_file=$(FileResolveName $ros_file)
    robinnp_file=$(FileExtensionIsCSV $G_ROBINNP_FILE)
    robinnp_file=$(FileResolveName $robinnp_file)
    echo -e "\nInput file(s): $(echo ${inputfiles[@]} | sed 's/ /, /g') (#$no_of_ros)\nOutput files: $ros_file, $robinnp_file\n"

    # CSV header
    FileWrite G_ROS_HASH CBOutputKey $ros_file
    FileWrite G_ROBINNP_HASH CBOutputKey $robinnp_file

    UtilProgressBar 0 $no_of_ros
    #The main-loop
    for inputfile in "${inputfiles[@]}"; do
	while read -r line || [[ -n "$line" ]]; do
            isOK=false
	    if [ -z "$line" ]; then :
	    else [[ "$line" =~ ^#.*$ ]] && continue
                isOK=true
		tmp=$(ROSRemoteExec $line)
		IFS=',' read -r -a DATA <<< "$tmp"
		if [ -z ${DATA[0]} ]; then
                    #If ${DATA[0]} doesn't exists, then report as an error
		    errorlist=("${errorlist[@]}" $line)
		else
		    HashInsert G_ROS_HASH "Equipment ID" $ros_equipment_id
		    HashInsert G_ROS_HASH "Serial Number" ${DATA[0]}
		    HashInsert G_ROS_HASH "MAC Address 1" ${DATA[1]}
		    HashInsert G_ROS_HASH "MAC Address 2" ${DATA[2]}
		    HashInsert G_ROS_HASH "Functional Name" ${DATA[3]}
		    HashInsert G_ROS_HASH "IPMI Module MAC Address" ${DATA[4]}
		    FileWrite G_ROS_HASH CBOutputValue $ros_file
		    for (( j=5; j<${#DATA[@]}; j+=2 )); do
			HashInsert G_ROBINNP_HASH "Equipment ID" $robinnp_equipment_id
			HashInsert G_ROBINNP_HASH "Parent" $ros_equipment_id
                        HashInsert G_ROBINNP_HASH "Functional Name" ${DATA[$j]}
			HashInsert G_ROBINNP_HASH "Assembler ID" ${DATA[$j]}
			HashInsert G_ROBINNP_HASH "Slot Index" ${DATA[$(($j+1))]}
			FileWrite G_ROBINNP_HASH CBOutputValue $robinnp_file
			robinnp_equipment_id=$(ROSResolveNextId $robinnp_equipment_id)
		    done
		    ros_equipment_id=$(ROSResolveNextId $ros_equipment_id)
		fi
	    fi
            ((isOK)) && {
	        UtilProgressBar $progress_num $no_of_ros
	        ((progress_num=progress_num+1))
            }
	done < "$inputfile"
    done
    echo -e "\n"
    if [ ${#errorlist[@]} -ne 0 ]; then echo -e "'Unknown' error occurred during SSH activity: $(echo ${errorlist[@]} | sed 's/ /, /g')\n"; fi
}

### ROS functions ###
function ROSRemoteExec
{   #Sends commands via SSH to ROS equipment
    #@param : $1=target
    echo `ssh -q -x -T -oStrictHostKeyChecking=no $USER@$1 'bash -s' < $G_ROS_DATA_SH 2> /dev/null`
}

function ROSResolveNextId
{   #Get next Equipment ID
    #@param : $1=Equipment ID
    if [ -z "$1" ]
    then echo
    else
        local hi=$(echo  $1 | sed s'/[0-9]*$//')
        local lo=$(echo ${1: ${#hi}})
        case $lo in ''|*[!0-9]*) return 1;; esac
        ((lo=lo+1))
        echo "${hi}${lo}"
    fi
    return 0
}


### FILE functions ###
function FileResolveName
{   #Resolves filenames. This to prevent overwriting of files
    #@param : $1=filename to resolve
    local nameis=${1%.*}
    local ext=$(echo $1|awk -F . '{print $NF}')
    local n=
    set -C
    until local newfile=$nameis${n:+-$n}.$ext
    { command exec 3> "$newfile"; } 2> /dev/null; do
        ((n++))
    done
    echo $newfile
}

function FileExists
{   #Test if file exists. If not, exit with Exit Code 1
    #@param : $1=path to file
    if [ ! -f "$1" ]; then
	echo "File '$1' does not exist!"
	exit 1
    fi
}

function FileExtensionIsCSV
{   #Add .csv file extension if needed
    #@param : $1=filename
    local name=$1
    if [[ $name != *.csv ]]; then
	name="${name}.csv"
    fi
    echo $name
}

function FileWrite
{   #Write to file (ACTUALLY append to file)
    #@param : $1=array, $2=callback, $3=file
    G_OUTPUT=''
    HashTraverse $1 $2
    echo $G_OUTPUT >> $3
}

### UTILITY functions ###
function UtilProgressBar
{   #Process data
    let _progress=(${1}*100/${2}*100)/100
    let _done=(${_progress}*4)/10
    let _left=40-$_done
    _fill=$(printf "%${_done}s")
    _empty=$(printf "%${_left}s")
    printf "\rProgress : [${_fill// /#}${_empty// /-}] ${_progress}%%"
}

function UtilShowHelp
{   #Print-out the help text
    #@param : -
    echo -e "Usage: $0 [OPTION]... [FILE]...\n\nOPTIONS:\n -h    Show this message\n"
}

### CALLBACK functions ###
function CBOutputKey
{   #Utility callback function. Output key(s)
    #@param : $1=key, $2=value
    local arg=$1;
    case "${arg}" in *[,]*) arg="\""${arg}"\"";; esac
    G_OUTPUT="${G_OUTPUT}${arg}${G_FIELD_SEPARATOR}"
}

function CBOutputValue
{   #Utility callback function. Output value(s)
    #@param : $1=key, $2=value
    local arg=$2;
    case "${arg}" in *[,]*) arg="\""${arg}"\"";; esac
    G_OUTPUT="${G_OUTPUT}${arg}${G_FIELD_SEPARATOR}"
}

### HASH functions ###
function HashInsert
{   #Insert a key-value pair into a hash table
    #\param $1 :the hash table
    #\param $2 :key of entry to insert (or update)
    #\param $3 :new value
    eval len='${#'$1'[@]}'
    for (( i=0; i<$len; i++)); do
        eval entry='${'$1'[$i]}'
        if [[ "${entry%%::*}" == $2 ]]; then break; fi
    done
    read $1'[i]' <<< $2::$3
}

function HashDelete
{   #Remove an entry from a hash table
    #\param $1 :the hash table
    #\param $2 :key of entry to remove
    eval len='${#'$1'[@]}'
    for (( i=0; i<$len; i++)); do
        eval entry='${'$1'[$i]}'
        if [[ "${entry%%::*}" == $2 ]]; then
            eval $1='(${'$1'[@]:0:$i} ${'$1'[@]:$(($i+1))})'; break #found it!
        fi
    done
}

function HashLookup
{   #Retrieve a key-value pair from a hash table
    #\param $1 :the hash table
    #\param $2 :key of entry to search for
    eval len='${#'$1'[@]}'
    for (( i=0; i<$len; i++)); do
        eval entry='${'$1'[$i]}'
        if [[ "${entry%%::*}" == $2 ]]; then #search for existing entry with this key
            echo "${entry##*::}"
            break
        fi
    done
}

function HashTraverse
{   #Walk over all entries in a hash table, calling callback function for each.
    #This is how you scan a whole Hashmap for its values.
    #\param $1 :the hash table to walk
    #\param $2 :the callback function
    #\param $3 :arbitrary data to pass along to the callback
    eval len='${#'$1'[@]}'
    for (( i=0; i<$len; i++)); do
        eval entry='${'$1'[$i]}'
        $2 "${entry%%::*}" "${entry##*::}" $3
    done
}

main "$@"
#EOF