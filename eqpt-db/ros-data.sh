#!/bin/bash


SERIAL=$(ipmitool -U ROSAdmin -P ROSAdmin -H $(hostname)-mgmt -I lanplus fru |grep Tag | awk '{print $5}')
HOST=$(hostname)
MAC0=$(sudo /usr/sbin/ethtool -P ctrl0 |awk '{print $3}')
MAC1=$(sudo /usr/sbin/ethtool -P ctrl0s1 |awk '{print $3}')
IPMIMAC=$(ipmitool -U ROSAdmin -P ROSAdmin -H $(hostname)-mgmt -I lanplus lan print |grep "MAC Address" | awk '{print $4}')
FPGAANDSLOTIDX=$(grep DNA /proc/robinnp   | awk '{print $6 " " $4+1}' | tr "\n " ",")

s=$(echo -n $SERIAL,$MAC0,$MAC1,$HOST,$IPMIMAC,$FPGAANDSLOTIDX)

c=","
n=$(grep -o "$c" <<< "$s" | wc -l)

echo -n $s

if [ $n -eq 6 ]; then
    echo ,
else
    echo
fi