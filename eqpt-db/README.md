__GenIIIROSAndRobinNPEquipmentDB__

 * Introduction
 * Requirements
 * How to run
 * Troubleshooting
 * FAQ
 * Maintainers
 * Notes


__INTRODUCTION__

This README describes how to use the *ros-main.sh* script.
1. This script will retrieve serial no., MAC-addresses and other data from ROS equipment.
2. The script will output two CSV files.
3. The purpose is then to upload the CSV files into the EquipmentDB.
  * The update interface for TDAQ computers: <br /> https://atglance.web.cern.ch/atglance/cgi-bin/update/showInsertInterfaceOnly?IIID=100
  * The update interface for TDAQ ROS ROBIN cards: <br /> https://atglance.web.cern.ch/atglance/cgi-bin/update/showInsertInterfaceOnly?IIID=103
  * The search interface in case you want to look for something: <br /> https://atglance.web.cern.ch/atglance/atlas_tdaq/main.php


__REQUIREMENTS__

The *ros-main.sh* script requires the following:
 * *ros-data.sh*
 * Equipment IDs, both for the ROS PCs and RobinNPs.
 * At least one file with a list of ROS PCs.

... and the file(s) should look similar to this one:
```
# It is allowed to use comments. Use comment '#' tag
pc-fwd-ros-afp-00
pc-fwd-ros-zdc-00
# pc-ibl-ros-b-00 <-- comment out!
pc-ibl-ros-b-01
# Is it OK to leave an empty line? Yes!

pc-ibl-ros-b-02
```

__HOW TO RUN__
1. Connecting via SSH to atlasgw: ```$ ssh atlasgw```
2. Do the steps asked by ATLAS application GATEWAY.
3. ```$ git clone <url>``` ^1 <br /> OR <br /> ```$ git pull```
4. Navigate to folder.
5. In order to give the permission to execute the script. Run the following command: ```$ chmod +x ros-main.sh```
6. To run the script, enter: ```$ ./ros-main.sh <list-of-ROS-PCs>```
7. Enter Equipment IDs ^2 - this ID will be the first, and the next ID will be: *id += 1*

example of a successful run
```
$ more ros-list.txt
pc-csc-ros-eca-00.cern.ch
pc-csc-ros-ecc-00.cern.ch
pc-fwd-ros-alfa-00.cern.ch
pc-fwd-ros-bcm-00.cern.ch
pc-fwd-ros-luc-00.cern.ch
--More--(4%)
$ ./ros-main.sh ros-list.txt
Enter ROS Equipment ID: 20DQCER3002067
Enter RobinNP Equipment ID: 20030841001030

Input file(s): ros-list.txt (#98)
Output files: ROS_Glance.csv, RobinNP_Glance.csv

Progress : [##############################----------] 77%
```
&nbsp;&nbsp;&nbsp;&nbsp;7 . Upload the CSV files into the EquipmentDB.
```
$ ls *.csv
RobinNP_Glance.csv  ROS_Glance.csv
```


__TROUBLESHOOTING__

1. Description: the *"File 'ros-data.sh' does not exist!"* __error message__ is triggered when the *ros-data.sh* is missing in folder.
    * Make sure that the *ros-main.sh* can find *ros-data.sh*.
    * Retrieve the script from the repository.
2. Description: the *"Missing input file(s)!"* __error message__ is triggered when an input file argument is missing.
    * You need to populate a file with ROS PCs (see section __REQUIREMENTS__) and pass it as an argument to *ros-main.sh*.
3. Description: the *"File 'myFaultyFilePath' does not exist!"* __error message__ is triggered when the user's input file is misspelled or missing.
    * Correct misspelling.
    * You need to populate a file with ROS PCs (see section __REQUIREMENTS__) and pass it as an argument to *ros-main.sh*.
4. Description: the *"Bad Equipment ID \<id\>"* __error message__ is triggered when the input ID-string doesn't contain a valid number.
    * The input ID-string must END WITH A NUMBER. Example: abc123
5. Description: the *"'Unknown' error occurred during SSH activity: \<TARGET\>... "* __error message__ is triggered when Data retrieval failed for that \<TARGET\>.
    * How to troubleshoot: ```$ ssh -x -T <TARGET> 'bash -s' < ros-data.sh```


__FAQ__

1. The *ros-main.sh* is asking for an Equipment ID. What is that? __The Equipment ID is a unique identification marking.__
2. How to create, delete or modify a column in the CSV-files? __Open the *ros-main.sh* before running it. At the beginning of the file, there are two Python-like dictionaries (see code snippet below) - please feel free to edit or make any changes you deem necessary.__

```sh
20 #Array pretending to be a Pythonic dictionary
21 declare -a G_ROS_HASH=(
22     #KEY::VALUE - the KEY is the CSV header, and the VALUE is the trailer
23 	   #For clarity, you must use :: as an alias for, to indicate the KEY/VALUE pairs
24     "Equipment ID::"
25     "Responsible::Wainer Vandelli"
26     "Location Details::"
27     "Model Name::MiriQuid"
```

__MAINTAINERS__

Current maintainer(s):
 * Mikael Kvist - mikael.stefan.kvist@cern.ch


__NOTES__

1. ^ It might be necessary to: run ```$ kinit```, and to __set KRB5__ as __clone URL__
2. ^ Example: 20DQCER3002067 and/or 20030841001030