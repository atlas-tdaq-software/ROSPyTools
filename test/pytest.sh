#!/bin/bash

# Pick up the runtime dependencies for the mock module 
# as they are not part of pytools in LCG
# The location of the dependencies is prepared and stored
# in EXTRA_PYTHONATH at the CMake level
# as they are not available at runtime
PYTHONPATH=${PYTHONPATH}:${EXTRA_PYTHONPATH}
tdaq_python -m unittest discover -s scripts
exit $?
