#!/usr/bin/env tdaq_python

from __future__ import print_function

import sys
from re import match, findall as regex
from collections import namedtuple
from itertools import dropwhile, islice


Channel = namedtuple('Channel', ['uid', 'cardidx', 'chidx'])


def findnondisabledresources(dbconnection, partition_name,
                             chtype='RobinDataChannel'):
    """
    Query the database non-disabled resources and select
    those of provided type.
    Due to the nature of the underlying algorithm, the function may
    return resources that are not liked to the partition.

    Returns a list of UIDs of non-disabled channels.
    """

    from subprocess import check_output

    cmd = ['dal_test_disabled', '-d', dbconnection, '-p', partition_name]

    if sys.version_info >= (3,0):
        resources = check_output(cmd, text=True)
    else:
        resources = check_output(cmd)

    # Find enabled 'RobinDataChannel' resources
    resources = (r for r in resources.splitlines()
                 if '@RobinDataChannel' in r and 'returns false' in r)

    # Fetch UIDs
    uids = [r.split()[4].replace("'", '').split('@')[0] for r in resources]

    return uids


def findokschannels(db_connection, robinnp_uid,
                    class_id="RobinNPDescriptorReadoutModule"):

    from config import Configuration

    db = Configuration(db_connection)

    robinnp = db.get_dal(class_id, robinnp_uid)

    cardidx = robinnp.PhysAddress

    channels = [Channel(l.id, cardidx, l.PhysAddress)
                for l in robinnp.Contains]

    return channels

# Check if argument is float
TRY_FLOAT = lambda f: float(f) if match(r'^[+-]?\d(>?\.\d+)?$', f) else None

# Helper class holding the status information off a s-link from the RobinNP
# point of view
Link = namedtuple('Link',
                  ['cardidx', 'channelidx', 'link_err', 'link_up',
                   'xoff', 'active', 'tlk_sync', 'test_mode', 'rol_emu',
                   'qsfpidx', 'qsfpchannelidx', 'rx_power', 'tx_bias',
                   'tx_err', 'tx_signal', 'rx_signal'])


def linkstatuses(procfile='/proc/robinnp'):
    """
    Parses the proc file and returns a list a Link instances

    """

    with open(procfile, 'rt') as proc:
        procdata = list(proc)

    # Remove \n
    procdata = (l.splitlines()[0] for l in procdata)

    # Find number of cards
    procdata = dropwhile(lambda x: 'Number of cards detected' not in x,
                         procdata)
    n_cards = int(next(procdata).split()[-1])

    # Find s-link status table
    procdata = dropwhile(lambda x: 'S-Link status bits' not in x, procdata)
    slink_states = list(islice(procdata, 2, 2 + n_cards * 12))

    # Find QSFP parameters table
    procdata = dropwhile(lambda x: 'QSFP | Vendor' not in x, procdata)
    qsfp_params = list(islice(procdata, 1, 1 + n_cards * 3))

    # Find cardidx and qsfpidx for Not Present QSFPs
    not_present = [regex(r'\w+', q)[:2]                       \
                       for q in qsfp_params if 'Not' in regex(r'\w+', q)[2]]

    # Find number of Not Present QSFPs
    n_not_present = len(not_present)

    # Find optic status table
    procdata = dropwhile(lambda x: 'Rx power  |  Tx bias' not in x, procdata)
    optic_states = list(islice(procdata, 1, 1 + n_cards * 12  \
                                   - n_not_present * 4))

    # Extend optic state with NaN entries - if needed
    # (NaN, standing for not a number)
    optic_states.extend(                                      \
        ['   %s  |   %s  |    %d    |     NaN mW |    NaN mA '\
             '|   NaN   |     NaN  /  NaN      |'             \
             % (cardidx, qfspidx, chidx)                      \
             for cardidx, qfspidx in not_present              \
             for chidx in range(4)])

    # Multiple key sort - sort optic state by multiple attributes
    optic_states = sorted(optic_states, key=lambda opt: (     \
            opt.split('|')[0], # cardidx
            opt.split('|')[1], # qsfpidx
            opt.split('|')[2]))# chidx

    # Merge s-link and optic state for each channel
    states = (' '.join(el) for el in zip(slink_states, optic_states))

    # Parse the states
    links = []
    for s in states:
        cardidx, chidx, linkerr, linkup, xoff, active, tlksync, test, emu,\
            _, qsfpidx, qsfpch, rxpower, _, txbias, _, txerr, notx, _, norx\
            = s.replace('|', '').split()

        l = Link(int(cardidx), int(chidx), linkerr == 'Yes', linkup == 'Yes',
                 xoff == 'Yes', active == 'Yes', tlksync == 'Yes',
                 test == 'Yes', emu == 'Yes', int(qsfpidx), int(qsfpch),
                 TRY_FLOAT(rxpower), TRY_FLOAT(txbias),
                 txerr != '0' if txerr.isdigit() else None,
                 notx == '0', norx == '0')
        links.append(l)

    return links


def do(partition_name, db, robinnp_uid):

    # Fetch all the information
    local = linkstatuses()
    enabled = findnondisabledresources(db, partition_name)
    oks_channels = findokschannels(db, robinnp_uid)

    # Keep only enabled channels
    oks_channels = [c for c in oks_channels if c.uid in enabled]

    failed = []

    for c in oks_channels:
        link = next(l for l in local if l.cardidx == c.cardidx
                    and l.channelidx == c.chidx)

        if link.link_err or not link.link_up or link.rx_power == 0. or\
                link.rx_power is None or not link.rx_signal or\
                not link.tx_signal or link.tx_err or\
                link.tx_err is None:
            failed.append(c.uid)
            msg = "Status check for link {} failed. Status array:\n{}"\
                .format(c.uid, link)
            print(msg, file=sys.stderr)

    return failed


if __name__ == '__main__':

    import os
    import argparse

    try:
        db = os.environ['TDAQ_DB']
    except KeyError as e:
        pass

    try:
        partition_name = os.environ['TDAQ_PARTITION']
    except KeyError as e:
        pass

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('robinnp_uid', type=str,
                        help='The OKS UID of the RobinNP to be tested')
    parser.add_argument('-p', '--partition_name',
                        help='The partition name')
    parser.add_argument('-d', '--db',
                        help='The database connection')

    args = parser.parse_args()

    db = args.db if args.db is not None else db
    partition_name = args.partition_name if args.partition_name is not None\
        else partition_name

    failed = do(partition_name, db, args.robinnp_uid)

    sys.exit(183 if failed else 0)


#####
# What follows is a test suite for the various functions in this file
####


import unittest
import mock
from io import StringIO

PROCROBINNP = ''.join([
    '\n',
    '\n',
    '\n',
    'RobinNP driver for release tdaq-06-01-01 (based on SVN tag , '
    'robinnp.c revision  )\n',
    'Build time: 09:49:43 Mar  1 2016\n',
    '\n',
    'Debug                       = 0\n',
    'Number of Dma pages         = 5000\n',
    'DMA page size               = 81920\n',
    'S-link Oscillator Frequency = 100\n',
    'Number of cards detected    = 2\n',
    '\n',
    'card |  In use  |\n',
    '   0 |      No  |\n',
    '   1 |      No  |\n',
    '\n',
    '\n',
    'card |  Interrupts handled\n',
    '   0 |\t       0 |\t       0 |\t       0 |\t  653426 |\t  614665 '
    '|\t 4836801 |\t 4548837 |\t       0 |\n',
    '   1 |\t       0 |\t       0 |\t       0 |\t  654590 |\t  654657 '
    '|\t 4629529 |\t 4590561 |\t       0 |\n',
    '\n',
    'card |  Interrupt flag\n',
    '   0 |             0 |             0 |             0 |             1 '
    '|             1 |             1 |             1 |             0 |\n',
    '   1 |             0 |             0 |             0 |             1 '
    '|             1 |             1 |             1 |             0 |\n',
    '\n',
    '\n',
    'Some other registers\n',
    'card |  PCI_STATUS | FPGA version | FPGA major | FPGA minor '
    '| # Channels | # ch. per sub-rob | # sub-robs |       BAR0 |\n',
    '   0 |      0x0010 |            1 |          6 |          8 '
    '|         12 |                 6 |          2 | 0xfb700000 |\n',
    '   1 |      0x0010 |            1 |          6 |          8 '
    '|         12 |                 6 |          2 | 0xfb600000 |\n',
    '\n',
    'S-Link status bits\n',
    'Card | ROL | Link err. | Link up | Xoff | Link active | TLK sync '
    '| Test mode | ROL Emulation |\n',
    '   0 |   0 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   0 |   1 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   0 |   2 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   0 |   3 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   0 |   4 |        Yes |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   0 |   5 |        No |     No |   No |          No |      Yes '
    '|        No |            No |\n',
    '   0 |   6 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   0 |   7 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   0 |   8 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   0 |   9 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   0 |  10 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   0 |  11 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   1 |   0 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   1 |   1 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   1 |   2 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   1 |   3 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   1 |   4 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   1 |   5 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   1 |   6 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   1 |   7 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   1 |   8 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   1 |   9 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   1 |  10 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   1 |  11 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '\n',
    '\n',
    'Memory modules\n',
    ' Card | Module | Part number        | Vendor | Serial # | Manufactured '
    '| Capacity |\n',
    '   0  |    0   | CT102464BF1339.C16 |  2c80  | 42300000 | week  0 2000 '
    '|    8 GB  |\n',
    '   0  |    1   | CT102464BF1339.C16 |  2c80  | 37210000 | week  0 2000 '
    '|    8 GB  |\n',
    '   1  |    0   | CT102464BF1339.C16 |  2c80  | 77250000 | week  0 2000 '
    '|    8 GB  |\n',
    '   1  |    1   | CT102464BF1339.C16 |  2c80  | 74250000 | week  0 2000 '
    '|    8 GB  |\n',
    '\n',
    ' \n',
    "FPGA 'DNA' card 0 = 0x4f26c04e0\n",
    "FPGA 'DNA' card 1 = 0x95a6c0460\n",
    ' \n',
    'FPGA temperature card 0 = 54 degrees celsius\n',
    'FPGA temperature card 1 = 62 degrees celsius\n',
    ' \n',
    'FPGA fan speed card 0 = 10278 RPM\n',
    'FPGA fan speed card 1 = 9765 RPM\n',
    ' \n',
    'FIFO Fill levels\n',
    ' card | SubRob | desc queue | First FIFO | Second FIFO | Common UPF '
    '|Host UPF space\n',
    '   0  |    0   |       0    |       0    |        0    |       0    '
    '|     32760\n',
    '   0  |    1   |       0    |       0    |        0    |       0    '
    '|     32760\n',
    '   1  |    0   |       0    |       0    |        0    |       0    '
    '|     32760\n',
    '   1  |    1   |       0    |       0    |        0    |       0    '
    '|     32760\n',
    '\n',
    ' card | ROL | Used FIFO | Free FIFO |\n',
    '   0  |  0  |       0   |       2   | \n',
    '   0  |  1  |       0   |       2   | \n',
    '   0  |  2  |       0   |       2   | \n',
    '   0  |  3  |       0   |       2   | \n',
    '   0  |  4  |       0   |       2   | \n',
    '   0  |  5  |       0   |       2   | \n',
    '   0  |  6  |       0   |       2   | \n',
    '   0  |  7  |       0   |       2   | \n',
    '   0  |  8  |       0   |       2   | \n',
    '   0  |  9  |       0   |       2   | \n',
    '   0  | 10  |       0   |       2   | \n',
    '   0  | 11  |       0   |       2   | \n',
    '   1  |  0  |       0   |       2   | \n',
    '   1  |  1  |       0   |       2   | \n',
    '   1  |  2  |       0   |       2   | \n',
    '   1  |  3  |       0   |       2   | \n',
    '   1  |  4  |       0   |       2   | \n',
    '   1  |  5  |       0   |       2   | \n',
    '   1  |  6  |       0   |       2   | \n',
    '   1  |  7  |       0   |       2   | \n',
    '   1  |  8  |       0   |       2   | \n',
    '   1  |  9  |       0   |       2   | \n',
    '   1  | 10  |       0   |       2   | \n',
    '   1  | 11  |       0   |       2   | \n',
    '\n',
    '\n',
    'QSFP parameters\n',
    ' Card | QSFP | Vendor          | Part number     | Rev. '
    '| Serial number   | Date code  | Temperature | Voltage |\n',
    '   0  |   0  | AVAGO           | AFBR-79EIDZ     |  01  '
    '| QD471616        | 19/11/2013 |  30 deg C   | 3.2768V |\n',
    '   0  |   1  | AVAGO           | AFBR-79EIDZ     |  01  '
    '| QD471621        | 19/11/2013 |  30 deg C   | 3.2768V |\n',
    '   0  |   2  | AVAGO           | AFBR-79EIDZ     |  01  '
    '| QC500574        | 15/12/2012 |  28 deg C   | 3.2768V |\n',
    '   1  |   0  | AVAGO           | AFBR-79EIDZ     |  01  '
    '| QD470955        | 18/11/2013 |  29 deg C   | 3.2768V |\n',
    '   1  |   1  | AVAGO           | AFBR-79EIDZ     |  01  '
    '| QC500700        | 16/12/2012 |  29 deg C   | 3.2768V |\n',
    '   1  |   2  | AVAGO           | AFBR-79EIDZ     |  01  '
    '| QC500729        | 16/12/2012 |  29 deg C   | 3.2768V |\n',
    '\n',
    ' Card | QSFP | channel |  Rx power  |  Tx bias  | TX Err. '
    '| Loss of Signal Tx/Rx |\n',
    '   0  |   0  |    0    |  0.0000 mW |  8.062 mA |    0    '
    '|      0   /   0       |\n',
    '   0  |   0  |    1    |  0.0684 mW |  8.224 mA |    0    '
    '|      0   /   0       |\n',
    '   0  |   0  |    2    |  0.0655 mW |  8.168 mA |    0    '
    '|      0   /   0       |\n',
    '   0  |   0  |    3    |  0.0661 mW |  7.278 mA |    0    '
    '|      0   /   0       |\n',
    '   0  |   1  |    0    |  0.2100 mW |  8.271 mA |    0    '
    '|      0   /   0       |\n',
    '   0  |   1  |    1    |  0.2313 mW |  7.685 mA |    0    '
    '|      0   /   0       |\n',
    '   0  |   1  |    2    |  0.2337 mW |  7.170 mA |    0    '
    '|      0   /   0       |\n',
    '   0  |   1  |    3    |  0.2178 mW |  7.577 mA |    0    '
    '|      0   /   0       |\n',
    '   0  |   2  |    0    |  0.2053 mW |  6.832 mA |    0    '
    '|      0   /   0       |\n',
    '   0  |   2  |    1    |  0.3158 mW |  6.794 mA |    0    '
    '|      0   /   0       |\n',
    '   0  |   2  |    2    |  0.3187 mW |  6.287 mA |    0    '
    '|      0   /   0       |\n',
    '   0  |   2  |    3    |  0.2255 mW |  6.652 mA |    0    '
    '|      0   /   0       |\n',
    '   1  |   0  |    0    |  0.0670 mW |  7.553 mA |    0    '
    '|      0   /   0       |\n',
    '   1  |   0  |    1    |  0.0867 mW |  7.491 mA |    0    '
    '|      0   /   0       |\n',
    '   1  |   0  |    2    |  0.0810 mW |  7.066 mA |    0    '
    '|      0   /   0       |\n',
    '   1  |   0  |    3    |  0.0757 mW |  6.840 mA |    0    '
    '|      0   /   0       |\n',
    '   1  |   1  |    0    |  0.1747 mW |  5.849 mA |    0    '
    '|      0   /   0       |\n',
    '   1  |   1  |    1    |  0.3028 mW |  5.827 mA |    0    '
    '|      0   /   0       |\n',
    '   1  |   1  |    2    |  0.2092 mW |  5.719 mA |    0    '
    '|      0   /   0       |\n',
    '   1  |   1  |    3    |  0.2241 mW |  5.862 mA |    0    '
    '|      0   /   0       |\n',
    '   1  |   2  |    0    |  0.1382 mW |  6.896 mA |    0    '
    '|      0   /   0       |\n',
    '   1  |   2  |    1    |  0.2389 mW |  6.946 mA |    0    '
    '|      0   /   0       |\n',
    '   1  |   2  |    2    |  0.2374 mW |  6.520 mA |    0    '
    '|      0   /   0       |\n',
    '   1  |   2  |    3    |  0.2150 mW |  7.030 mA |    1    '
    '|      0   /   0       |\n',
    ' \n',
    "The command 'echo <action> > /proc/robinnp', executed as root,\n",
    'allows you to interact with the driver. Possible actions are:\n',
    'debug   -> enable debugging\n',
    'nodebug -> disable debugging\n',
    'elog    -> Log errors to /var/log/message\n',
    'noelog  -> Do not log errors to /var/log/message\n'])

PROCROBINNP_QSFPNOTPRESENT_TEST01 = ''.join([
    '\n',
    '\n',
    '\n',
    'RobinNP driver for release tdaq-06-01-01 (based on SVN tag '
    'ROSRCDdrivers-00-00-99, robinnp.c revision )\n',
    'Build time: 08:03:46 Mar  6 2017\n',
    '\n',
    'Debug                       = 0\n',
    'Number of Dma pages         = 5000\n',
    'DMA page size               = 81920\n',
    'S-link Oscillator Frequency = 100\n',
    'Number of cards detected    = 2\n',
    '\n',
    'card |  In use  |\n',
    '   0 |      No  |\n',
    '   1 |      No  |\n',
    '\n',
    '\n',
    'card |  Interrupts handled\n',
    '   0 |	       0 |	       0 |	       0 |	       0 |	       0 '
    '|	       0 |	       0 |	       0 |\n',
    '   1 |	       0 |	       0 |	       0 |	       0 |	       0 '
    '|	       0 |	       0 |	       0 |\n',
    '\n',
    'card |  Interrupt flag\n',
    '   0 |             0 |             0 |             0 |             0 '
    '|             0 |             0 |             0 |             0 |\n',
    '   1 |             0 |             0 |             0 |             0 '
    '|             0 |             0 |             0 |             0 |\n',
    '\n',
    '\n',
    'Some other registers\n',
    'card |  PCI_STATUS | FPGA version | FPGA major | FPGA minor '
    '| # Channels | # ch. per sub-rob | # sub-robs |       BAR0 |\n',
    '   0 |      0x0010 |            1 |          6 |         10 '
    '|         12 |                 6 |          2 | 0xfb700000 |\n',
    '   1 |      0x0010 |            1 |          6 |         10 '
    '|         12 |                 6 |          2 | 0xfb600000 |\n',
    '\n',
    'S-Link status bits\n',
    'Card | ROL | Link err. | Link up | Xoff | Link active | TLK sync '
    '| Test mode | ROL Emulation |\n',
    '   0 |   0 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   0 |   1 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   0 |   2 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   0 |   3 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   0 |   4 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   0 |   5 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   0 |   6 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   0 |   7 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   0 |   8 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   0 |   9 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   0 |  10 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   0 |  11 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   1 |   0 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   1 |   1 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   1 |   2 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   1 |   3 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   1 |   4 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   1 |   5 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   1 |   6 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   1 |   7 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   1 |   8 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   1 |   9 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   1 |  10 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   1 |  11 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '\n',
    '\n',
    'Memory modules\n',
    ' Card | Module | Part number        | Vendor | Serial # | Manufactured '
    '| Capacity |\n',
    '   0  |    0   | 8KTF51264HZ-1G6E1  |  2c80  | 3364bad2 | week  9 2014 '
    '|    4 GB  |\n',
    '   0  |    1   | 8KTF51264HZ-1G6E1  |  2c80  | 3364bad3 | week  9 2014 '
    '|    4 GB  |\n',
    '   1  |    0   | 8KTF51264HZ-1G6E1  |  2c80  | 3364bae5 | week  9 2014 '
    '|    4 GB  |\n',
    '   1  |    1   | 8KTF51264HZ-1G6E1  |  2c80  | 3364bae7 | week  9 2014 '
    '|    4 GB  |\n',
    '\n',
    ' Error: Card 0 subrob 1 memory not initialised\n',
    ' \n',
    "FPGA 'DNA' card 0 = 0x7176c3510\n",
    "FPGA 'DNA' card 1 = 0x4576c1090\n",
    ' \n',
    'FPGA temperature card 0 = 50 degrees celsius\n',
    'FPGA temperature card 1 = 50 degrees celsius\n',
    ' \n',
    'FPGA fan speed card 0 = 10278 RPM\n',
    'FPGA fan speed card 1 = 9765 RPM\n',
    ' \n',
    'FIFO Fill levels\n',
    ' card | SubRob | desc queue | First FIFO | Second FIFO | Common UPF '
    '|Host UPF space\n',
    '   0  |    0   |       0    |       0    |        0    |       0    '
    '|     32760\n',
    '   0  |    1   |       0    |       0    |        0    |       0    '
    '|     32760\n',
    '   1  |    0   |       0    |       0    |        0    |       0    '
    '|     32760\n',
    '   1  |    1   |       0    |       0    |        0    |       0    '
    '|     32760\n',
    '\n',
    ' card | ROL | Used FIFO | Free FIFO |\n',
    '   0  |  0  |       0   |       2   | \n',
    '   0  |  1  |       0   |       2   | \n',
    '   0  |  2  |       0   |       2   | \n',
    '   0  |  3  |       0   |       2   | \n',
    '   0  |  4  |       0   |       2   | \n',
    '   0  |  5  |       0   |       2   | \n',
    '   0  |  6  |       0   |       2   | \n',
    '   0  |  7  |       0   |       2   | \n',
    '   0  |  8  |       0   |       2   | \n',
    '   0  |  9  |       0   |       2   | \n',
    '   0  | 10  |       0   |       2   | \n',
    '   0  | 11  |       0   |       2   | \n',
    '   1  |  0  |       0   |       2   | \n',
    '   1  |  1  |       0   |       2   | \n',
    '   1  |  2  |       0   |       2   | \n',
    '   1  |  3  |       0   |       2   | \n',
    '   1  |  4  |       0   |       2   | \n',
    '   1  |  5  |       0   |       2   | \n',
    '   1  |  6  |       0   |       2   | \n',
    '   1  |  7  |       0   |       2   | \n',
    '   1  |  8  |       0   |       2   | \n',
    '   1  |  9  |       0   |       2   | \n',
    '   1  | 10  |       0   |       2   | \n',
    '   1  | 11  |       0   |       2   | \n',
    '\n',
    '\n',
    'QSFP parameters\n',
    ' Card | QSFP | Vendor          | Part number     | Rev. '
    '| Serial number   | Date code  | Temperature | Voltage |\n',
    '   0  |   0  | AVAGO           | AFBR-79EIDZ     |  01  '
    '| QE133457        | 25/03/2014 |  29 deg C   | 3.2768V |\n',
    '   0  |   1  | AVAGO           | AFBR-79EIDZ     |  01  '
    '| QE133446        | 25/03/2014 |  29 deg C   | 3.2768V |\n',
    '   0  |   2  | 	***  Not Present  ***\n',
    '   1  |   0  | AVAGO           | AFBR-79EIDZ     |  01  '
    '| QE133463        | 25/03/2014 |  26 deg C   | 3.2768V |\n',
    '   1  |   1  | AVAGO           | AFBR-79EIDZ     |  01  '
    '| QE133461        | 25/03/2014 |  26 deg C   | 3.2768V |\n',
    '   1  |   2  | 	***  Not Present  ***\n',
    '\n',
    ' Card | QSFP | channel |  Rx power  |  Tx bias  | TX Err. '
    '| Loss of Signal Tx/Rx |\n',
    '   0  |   0  |    0    |  0.4911 mW |  6.652 mA |    0    '
    '|      0   /   0       |\n',
    '   0  |   0  |    1    |  0.5129 mW |  6.515 mA |    0    '
    '|      0   /   0       |\n',
    '   0  |   0  |    2    |  0.0000 mW |  6.380 mA |    0    '
    '|      0   /   1       |\n',
    '   0  |   0  |    3    |  0.0000 mW |  6.239 mA |    0    '
    '|      0   /   1       |\n',
    '   0  |   1  |    0    |  0.0000 mW |  6.526 mA |    0    '
    '|      0   /   1       |\n',
    '   0  |   1  |    1    |  0.0000 mW |  6.477 mA |    0    '
    '|      0   /   1       |\n',
    '   0  |   1  |    2    |  0.5262 mW |  6.745 mA |    0    '
    '|      0   /   0       |\n',
    '   0  |   1  |    3    |  0.4889 mW |  6.635 mA |    0    '
    '|      0   /   0       |\n',
    '   1  |   0  |    0    |  0.4922 mW |  7.323 mA |    0    '
    '|      0   /   0       |\n',
    '   1  |   0  |    1    |  0.3896 mW |  7.219 mA |    0    '
    '|      0   /   0       |\n',
    '   1  |   0  |    2    |  0.0000 mW |  6.559 mA |    0    '
    '|      0   /   1       |\n',
    '   1  |   0  |    3    |  0.0000 mW |  6.493 mA |    0    '
    '|      0   /   1       |\n',
    '   1  |   1  |    0    |  0.0000 mW |  7.449 mA |    0    '
    '|      0   /   1       |\n',
    '   1  |   1  |    1    |  0.0000 mW |  7.073 mA |    0    '
    '|      0   /   1       |\n',
    '   1  |   1  |    2    |  0.5261 mW |  7.072 mA |    0    '
    '|      0   /   0       |\n',
    '   1  |   1  |    3    |  0.4893 mW |  6.637 mA |    0    '
    '|      0   /   0       |\n',
    ' \n',
    "The command 'echo <action> > /proc/robinnp', executed as root,\n",
    'allows you to interact with the driver. Possible actions are:\n',
    'debug   -> enable debugging\n',
    'nodebug -> disable debugging\n',
    'elog    -> Log errors to /var/log/message\n',
    'noelog  -> Do not log errors to /var/log/message\n'])

PROCROBINNP_QSFPNOTPRESENT_TEST02 = ''.join([
    '\n',
    '\n',
    '\n',
    'RobinNP driver for release tdaq-06-01-01 (based on SVN tag '
    'ROSRCDdrivers-00-00-99, robinnp.c revision )\n',
    'Build time: 08:03:46 Mar  6 2017\n',
    '\n',
    'Debug                       = 0\n',
    'Number of Dma pages         = 5000\n',
    'DMA page size               = 81920\n',
    'S-link Oscillator Frequency = 100\n',
    'Number of cards detected    = 1\n',
    '\n',
    'card |  In use  |\n',
    '   0 |     Yes  |\n',
    '\n',
    '\n',
    'card |  Interrupts handled\n',
    '   0 |	       0 |	       0 |	       0 |	10079014514 |	       0 '
    '|	4567957911 |	       0 |	       0 |\n',
    '\n',
    'card |  Interrupt flag\n',
    '   0 |             0 |             0 |             0 |             0 '
    '|             0 |             0 |             0 |             0 |\n',
    '\n',
    '\n',
    'Some other registers\n',
    'card |  PCI_STATUS | FPGA version | FPGA major | FPGA minor '
    '| # Channels | # ch. per sub-rob | # sub-robs |       BAR0 |\n',
    '   0 |      0x0010 |            1 |          6 |         10 '
    '|         12 |                 6 |          2 | 0xfbe00000 |\n',
    '\n',
    'S-Link status bits\n',
    'Card | ROL | Link err. | Link up | Xoff | Link active | TLK sync '
    '| Test mode | ROL Emulation |\n',
    '   0 |   0 |        No |     Yes |   No |          No |      Yes '
    '|        No |            No |\n',
    '   0 |   1 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   0 |   2 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   0 |   3 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   0 |   4 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   0 |   5 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   0 |   6 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   0 |   7 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   0 |   8 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   0 |   9 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   0 |  10 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '   0 |  11 |        No |      No |   No |          No |       No '
    '|        No |            No |\n',
    '\n',
    '\n',
    'Memory modules\n',
    ' Card | Module | Part number        | Vendor | Serial # | Manufactured '
    '| Capacity |\n',
    '   0  |    0   | 8KTF51264HZ-1G6E1  |  2c80  | 3364bb16 | week  9 2014 '
    '|    4 GB  |\n',
    '   0  |    1   | 8KTF51264HZ-1G6E1  |  2c80  | 3364bb23 | week  9 2014 '
    '|    4 GB  |\n',
    '\n',
    ' \n',
    "FPGA 'DNA' card 0 = 0x7176c37a0\n",
    ' \n',
    'FPGA temperature card 0 = 45 degrees celsius\n',
    ' \n',
    'FPGA fan speed card 0 = 10850 RPM\n',
    ' \n',
    'FIFO Fill levels\n',
    ' card | SubRob | desc queue | First FIFO | Second FIFO | Common UPF '
    '|Host UPF space\n',
    '   0  |    0   |       0    |       0    |        0    |       0    '
    '|     32759\n',
    '   0  |    1   |       0    |       0    |        0    |       0    '
    '|     32760\n',
    '\n',
    ' card | ROL | Used FIFO | Free FIFO |\n',
    '   0  |  0  |       0   |      12   | \n',
    '   0  |  1  |       0   |       2   | \n',
    '   0  |  2  |       0   |       2   | \n',
    '   0  |  3  |       0   |       2   | \n',
    '   0  |  4  |       0   |       2   | \n',
    '   0  |  5  |       0   |       2   | \n',
    '   0  |  6  |       0   |       2   | \n',
    '   0  |  7  |       0   |       2   | \n',
    '   0  |  8  |       0   |       2   | \n',
    '   0  |  9  |       0   |       2   | \n',
    '   0  | 10  |       0   |       2   | \n',
    '   0  | 11  |       0   |       2   | \n',
    '\n',
    '\n',
    'QSFP parameters\n',
    ' Card | QSFP | Vendor          | Part number     | Rev. '
    '| Serial number   | Date code  | Temperature | Voltage |\n',
    '   0  |   0  | AVAGO           | AFBR-79EIDZ     |  01  '
    '| QE135541        | 27/03/2014 |  25 deg C   | 3.2768V |\n',
    '   0  |   1  | 	***  Not Present  ***\n',
    '   0  |   2  | 	***  Not Present  ***\n',
    '\n',
    ' Card | QSFP | channel |  Rx power  |  Tx bias  | TX Err. '
    '| Loss of Signal Tx/Rx |\n',
    '   0  |   0  |    0    |  0.3164 mW |  6.650 mA |    0    '
    '|      0   /   0       |\n',
    '   0  |   0  |    1    |  0.0000 mW |  6.595 mA |    0    '
    '|      0   /   1       |\n',
    '   0  |   0  |    2    |  0.0000 mW |  6.342 mA |    0    '
    '|      0   /   1       |\n',
    '   0  |   0  |    3    |  0.0000 mW |  6.141 mA |    0    '
    '|      0   /   1       |\n',
    ' \n',
    "The command 'echo <action> > /proc/robinnp', executed as root,\n",
    'allows you to interact with the driver. Possible actions are:\n',
    'debug   -> enable debugging\n',
    'nodebug -> disable debugging\n',
    'elog    -> Log errors to /var/log/message\n',
    'noelog  -> Do not log errors to /var/log/message\n'])

class ContextualStringIO(StringIO):

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.close()
        return False


class Test(unittest.TestCase):

    DB = 'oksconfig:test/part.data.xml'
    PART = 'part_test'

    def test01_procrobinp(self):

        with mock.patch("test_slink.open",
                        return_value=ContextualStringIO(PROCROBINNP),
                        create=True):
            links = linkstatuses()

        self.assertEqual(len(links), 24)

        l = next(l for l in links if l.cardidx == 1 and l.channelidx == 1)

        self.assertEqual(l.qsfpidx, 0)
        self.assertEqual(l.qsfpchannelidx, 1)

        self.assertEqual(l.rx_power, 0.0867)
        self.assertTrue(l.link_up)

        l = next(l for l in links if l.cardidx == 0 and l.channelidx == 4)
        self.assertTrue(l.link_up)
        self.assertTrue(l.link_err)

        l = next(l for l in links if l.cardidx == 0 and l.channelidx == 5)
        self.assertFalse(l.link_up)
        self.assertFalse(l.link_err)

    def test02_qsfpnotpresent01(self):
        with mock.patch("test_slink.open",
                        return_value=ContextualStringIO(\
                        PROCROBINNP_QSFPNOTPRESENT_TEST01),
                        create=True):
            links = linkstatuses()

        self.assertEqual(len(links), 24)

        l = next(l for l in links if l.cardidx == 1 and l.channelidx == 1)

        self.assertEqual(l.qsfpidx, 0)
        self.assertEqual(l.qsfpchannelidx, 1)

        self.assertEqual(l.rx_power, 0.3896)
        self.assertTrue(l.link_up)

        l = next(l for l in links if l.cardidx == 0 and l.channelidx == 4)
        self.assertFalse(l.link_up)
        self.assertEqual(l.rx_power, 0.0000)

        l = next(l for l in links if l.cardidx == 0 and l.channelidx == 5)
        self.assertFalse(l.link_up)
        self.assertFalse(l.link_err)

        l = next(l for l in links if l.cardidx == 1 and l.channelidx == 11)
        self.assertFalse(l.link_up)
        self.assertIsNone(l.rx_power)

        l = next(l for l in links if l.cardidx == 0 and l.channelidx == 8)
        self.assertIsNone(l.tx_bias)
        self.assertIsNone(l.tx_err)

    def test03_qsfpnotpresent02(self):
        with mock.patch("test_slink.open",
                        return_value=ContextualStringIO(\
                        PROCROBINNP_QSFPNOTPRESENT_TEST02),
                        create=True):
            links = linkstatuses()

        self.assertEqual(len(links), 12)

        l = next(l for l in links if l.cardidx == 0 and l.channelidx == 0)

        self.assertEqual(l.qsfpidx, 0)
        self.assertEqual(l.qsfpchannelidx, 0)

        self.assertEqual(l.rx_power, 0.3164)
        self.assertTrue(l.link_up)

        l = next(l for l in links if l.cardidx == 0 and l.channelidx == 1)
        self.assertFalse(l.link_up)
        self.assertEqual(l.rx_power, 0.0000)

        l = next(l for l in links if l.cardidx == 0 and l.channelidx == 6)
        self.assertFalse(l.link_up)
        self.assertIsNone(l.rx_power)

        l = next(l for l in links if l.cardidx == 0 and l.channelidx == 11)
        self.assertIsNone(l.tx_bias)
        self.assertIsNone(l.tx_err)

    def test04_enabledlinks(self):

        enabled_channels = findnondisabledresources(Test.DB, Test.PART)

        self.assertFalse('ROL-IBL-B-00-140083' in enabled_channels)
        self.assertFalse('ROL-IBL-B-00-140113' in enabled_channels)

    def test05_oks(self):

        db = 'oksconfig:test/part.data.xml'

        channels = findokschannels(db, 'ROBIN-IBL-B-00-0')

        self.assertTrue('ROL-IBL-B-00-140060' in [c.uid for c in channels])
        self.assertTrue('ROL-IBL-B-00-140083' in [c.uid for c in channels])
        self.assertFalse('ROL-IBL-B-00-140113' in [c.uid for c in channels])

        ch = next(c for c in channels if c.uid == 'ROL-IBL-B-00-140083')

        self.assertEqual(ch.cardidx, 0)
        self.assertEqual(ch.chidx, 11)

        with self.assertRaises(RuntimeError):
            findokschannels(db, 'DOES_NOT_EXIST')

    def test06_do(self):

        with mock.patch("test_slink.open",
                        return_value=ContextualStringIO(PROCROBINNP),
                        create=True):

            robinnp_uid = 'ROBIN-IBL-B-00-0'

            failed = do(Test.PART, Test.DB, robinnp_uid)

            self.assertEqual(len(failed), 3)
            self.assertTrue('ROL-IBL-B-00-140070' in failed)
            self.assertTrue('ROL-IBL-B-00-140071' in failed)

        with mock.patch("test_slink.open",
                        return_value=ContextualStringIO(PROCROBINNP),
                        create=True):

            robinnp_uid = 'ROBIN-IBL-B-00-1'

            failed = do(Test.PART, Test.DB, robinnp_uid)

            self.assertEqual(len(failed), 0)
