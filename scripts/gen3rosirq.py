#!/usr/bin/env python

from itertools import cycle
from math import ceil
import glob
import sys
import os.path

def setaffinity(irq, coreidx, ncores, force=False):

    digits = int(ceil(1. * ncores / 4))
    cpumask = '{0:x}'.format(1 << coreidx).zfill(digits)
    affinity = '/proc/irq/%s/smp_affinity' % irq

    with open(affinity, 'r') as affinityfile:
        current_affinity = affinityfile.readline().rstrip('\n')

    with open(affinity, 'w') as affinityfile:
        write = force or current_affinity != cpumask
        if write:
            print('Updating')
            affinityfile.write(cpumask)


if __name__ == '__main__':

    MELLANOX_ID = '0x15b3'

    # Find all the Mellanox devices using the vendor ID
    # Avoid double counting them (one device has two interfaces)
    # We use a dictionary with key the interfaces associated to the card
    mellanox_cards = {}
    for device_vendor in glob.glob('/sys/class/net/*/device/vendor'):

        with open(device_vendor) as vendor_file:
            if vendor_file.readline().rstrip('\n') != MELLANOX_ID:
                continue

        device_path = os.path.dirname(device_vendor)
        interfaces = [os.path.basename(f)
                      for f in glob.glob(os.path.join(device_path, 'net/*'))]

        mellanox_cards[tuple(interfaces)] = device_path


    mellanox_cards = mellanox_cards.values()

    # Find the list of interrupts for each card. Sort them also
    device_irqs = []
    for device in mellanox_cards:
        interrupts = (os.path.basename(f)
                      for f in glob.glob(os.path.join(device, 'msi_irqs/*')))

        interrupts = sorted(int(i) for i in interrupts)

        device_irqs.append(interrupts)

    # Find the RobinNP interrupts
    with open('/proc/interrupts', 'r') as f:
        irqmap = [l for l in f]

    robinirq = [l.split()[0].strip(':') for l in irqmap if 'robinnp' in l]
    robinirq.sort(key=int)

    # Find the number of cores
    with open('/proc/cpuinfo', 'r') as cpuinfo:
        ncores = len([l for l in cpuinfo if 'processor	:' in l])

    # Distribute Robin IRQ uniformely on the available cores
    for idx, irq in enumerate(robinirq):
        coreidx = idx % ncores
        print(coreidx, irq)
        setaffinity(irq, coreidx, ncores)

    # For the Mellanox cards we expect 25 interrups for each cards
    # One is a common interrupt (the first one), followed by two sets
    # of twelve, one set for each port. Unfortunately we have to rely
    # on ordering
    netirqs = (device_irqs[0][:13], device_irqs[0][13:25],
               device_irqs[1][:13], device_irqs[1][13:25])

    # Group eth irqs into distinct subsets of the CPU cores
    # Simple approximation, does not work for all PCs
    # it works for the Gen3 PC model
    subsetsize = ncores // len(netirqs)

    for idx, irqs in enumerate(netirqs):
        startidx = idx * subsetsize
        cores = range(startidx, startidx + subsetsize)

        for irq, coreidx in zip(irqs, cycle(cores)):
            print(irq, coreidx)
            setaffinity(irq, coreidx, ncores)
