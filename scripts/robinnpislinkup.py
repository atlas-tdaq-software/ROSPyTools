"""Usage: python robinnpislinkup.py <path to gen3rospclayout.csv>"""
import sys, csv, re
import subprocess as sp

class CONST(object):
    # Position
    LINK_UP = 3
    ROS_HOST = 1
    RX_POWER = 3

def main(gen3rospclayout):
    """This is main"""
    dict_ = {}

    with open(gen3rospclayout, 'r') as csvfile:
        reader = csv.reader(csvfile)
        roslist = [row[CONST.ROS_HOST] for row in list(reader)\
                       if row and "pc" in row[CONST.ROS_HOST]]

    for ros in roslist:
        print(ros)
        returncode, stdout, stderr = secureshell(ros, "cat /proc/robinnp")

        params = parserobinnp(stdout, "Serial number")
        linkup = parserobinnp(stdout, "Link up", CONST.LINK_UP)
        rxpower = parserobinnp(stdout, "Rx power", CONST.RX_POWER)

        # Index building
        indexes = [int(r[0])*12+(int(r[1])*4)+j for i, r in enumerate(params)\
                       if not "Not" in r[2] for j in range(4)]

        for idx, pos in enumerate(indexes):
            if float(rxpower[idx]) and "No" in linkup[pos]:
                dict_[ros] = stdout
                break

    for ros, stdout in dict_.items():
        input("\n\n" + ros + "\nPress Enter to continue...")
        print(stdout)
    for ros, _ in dict_.items():
        print(ros)

def secureshell(host, command):
    """Perform commands over ssh"""
    ssh = sp.Popen(["/bin/bash", "-c",
                    "ssh -o StrictHostKeyChecking=no %s %s" % (host, command)],
                   stdout=sp.PIPE, stderr=sp.STDOUT)
    stdout, stderr = ssh.communicate()
    return (ssh.returncode, stdout, stderr)

def parserobinnp(catprocrobinnp, regex, pos=None):
    """cat /proc/robinnp"""
    result = []
    match = False
    for line in catprocrobinnp.split('\n'):
        if regex in line:
            match = True
        elif not line.strip():
            match = False
        elif match:
            result.append(re.findall('[0-9a-zA-Z_.]+', line))
    return [(row[pos] if pos else row) for row in result]

if __name__ == "__main__":
    try:
        sys.argv[1]
    except IndexError:
        print("Usage: python robinnpislinkup.py <path to gen3rospclayout.csv>")
        exit(1)
    main(sys.argv[1])

# EOF
