#!/usr/bin/env tdaq_python

import os
import os.path
import argparse
import time
import sys
import signal
import threading

from itertools import groupby
from collections import namedtuple
from operator import itemgetter, truth

import ROOT
from ispy import IPCPartition, ISCriteria, ISInfoIterator, ISObject
from oh import OHRootProvider
import ers


class SignalHandler(object):

    def __init__(self, wakeup):
        self.__fired = False
        self.__wakeup = wakeup
        signal.signal(signal.SIGINT, self._handler)
        signal.signal(signal.SIGTERM, self._handler)

    def _handler(self, signum, frame):
        self.__fired = True
        with self.__wakeup:
            self.__wakeup.notify()

    def __bool__(self):
        return self.__fired


class DataSet(object):

    def __init__(self, values, labels, attributes):
        
        self.attributes = attributes
        self.data = list(zip(labels, values))

    def __bool__(self):
        return truth(self.data)
        
    def subset(self, key):
        sub = DataSet([], [], self.attributes)
        sub.data = [el for el in self.data if key(el)]
        
        return sub

    def checkout(self):
        for _,v in self.data:
            try:
                v.checkout()
            except:
                pass

    def extend(self, ds):
        if self.attributes != ds.attributes:
            raise ValueError(
                'Cannot extend from a DataSet with different attributes')

        self.data.extend(ds.data)
            

def fetchISobject(partition, isserver, regex, infotype):

    it = ISInfoIterator(partition, isserver, ISCriteria(regex))

    objs = []
    while it.next():
        objs.append(ISObject(partition, it.name(), infotype))
        objs[-1].checkout()

    return objs


def fetchROSs(partition, serverRC):

    return fetchISobject(partition, serverRC, "ROS-.*", "RCStateInfo")


def getISattributes(obj):

    attrs = dir(obj)
    attrs.remove('time')
    attrs.remove('type')

    # Attributes must be unique, so we use a 'set'
    return set(attrs)


def publish_histos(dataset, folder, provider):

    if not dataset:
        # Skip publication if the dataset is empty
        return

    for attr in dataset.attributes:

        h = ROOT.TH1F(attr, attr, 1, -0.5, 0.5)
        
        for label, el in dataset.data:
            h.Fill(label, getattr(el, attr))

        h.LabelsDeflate('X')
        provider.publish(h, os.path.join(folder, h.GetName()))


def prepare_rm_dataset(readoutmodule, attrs, ROLData):

    ds = DataSet([], [], attrs)
    for el in readoutmodule:

        # Checkout new values
        el.checkout()

        # Transpose the RobinNP IS matrix
        tmp_data = zip(*[getattr(el, attr) for attr in attrs])
        tmp_data = [ROLData(*d) for d in tmp_data]

        # Now 'tmp_data' contains one row of values for
        # each channel

        # Create a label for each channel
        infoname = el.name()
        robinnp_idx = int(infoname[-1])
        label = "%s_%02d" % (infoname.split('.')[2], robinnp_idx)
        tmp_labels = ["%s_%02d" %
                      (label, idx) for idx in range(len(tmp_data))]

        tmp_ds = DataSet(tmp_data, tmp_labels, attrs)

        # Drop channels and labels that are not enabled
        tmp_ds = tmp_ds.subset(lambda x: x[1].rolEnabled)

        ds.extend(tmp_ds)

    return ds


def initialise(triggerin, readoutmodule):

    # Here we checkout the IS data for the first time
    # and create all the data structures needed later

    for t in triggerin:
        t.checkout()

    for r in list(readoutmodule):
        try:
            r.checkout()
        except ers.Issue as e:
            if e.isInstanceOf('InfoNotFound'):
                # Not all modules are available as a ROS may
                # have a single RobinNP
                readoutmodule.remove(r)
            else:
                raise e

    ti_attrs = rm_attrs = ROLData = None

    if triggerin:
        ti_attrs = getISattributes(triggerin[0])

    if readoutmodule:
        rm_attrs = getISattributes(readoutmodule[0])
        # Create a data structure for the unfolded RobinNP information
        ROLData = namedtuple('ROLData', rm_attrs)

    return triggerin, readoutmodule, ti_attrs, rm_attrs, ROLData


def detector_match(det):

    def match(el):
        return det == el[0].split('-')[1]

    return match


def partition_match(part):

    def match(el):
        return part == el[0].split('-')[2]

    return match


def run(args):

    partition = IPCPartition(args.partition)
    period = args.period
    isserver = args.is_server
    serverRC = args.rc_server

    # Drop all the histograms from previous instances
    rosmon_histos = fetchISobject(partition, args.oh_server,
                                  args.provider_name + './.*',
                                  'HistogramData<float>')
    for el in rosmon_histos:
        el.remove()

    # Create the OHProvider
    ohprovider = OHRootProvider(partition, args.oh_server,
                                args.provider_name, None)
    
    # Create a list for the running ROS and the labels associated
    runningROS = fetchROSs(partition, serverRC)
    labelsRunningROS = [r.name().split('.')[1] for r in runningROS]

    labelsRunningROS.sort()
    detectors = set(label.split('-')[1] for label in labelsRunningROS)
    partitions = dict((k, set(map(itemgetter(1), g)))
                      for k, g in
                      groupby((label.split('-')[1:3]
                               for label in labelsRunningROS), itemgetter(0)))

    # Create the TriggerIn ISObjects
    triggerin_isname = isserver + '.ROS.%s.TriggerIn0'
    triggerin = [ISObject(partition, triggerin_isname % ros, 'NPTriggerInfo')
                 for ros in labelsRunningROS]

    # Create the ReadoutModule ISObjects
    readoutmodule_isname = isserver + '.' + 'ROS.%s.ReadoutModule%d'
    readoutmodule = [ISObject(partition, readoutmodule_isname % (ros, idx),
                              'RobinNPDescriptorReadoutModuleInfo')
                     for ros in labelsRunningROS for idx in (0, 1)]

    triggerin, readoutmodule, ti_attrs, rm_attrs, ROLData =\
        initialise(triggerin, readoutmodule)

    # TriggerIn lists are constant, we can define them once
    # (ReadoutModule are not because of the stopless removal)
    labels = [t.name().split('.')[2] for t in triggerin]
    
    triggerin = DataSet(triggerin, labels, ti_attrs)

    ti_det = [(det, triggerin.subset(detector_match(det)))
              for det in detectors]
    
    ti_part = [(det, part, ds.subset(partition_match(part)))
               for det,ds in ti_det for part in partitions[det]]

    while not args.stophandler:

        start = time.time()

        # Update the TriggerIn data
        triggerin.checkout()

        # Publish global TriggerIn
        publish_histos(triggerin, "/GLOBAL/TriggerIn", ohprovider)

        # Publish per detector TriggerIn
        for det, ds in ti_det:
            publish_histos(ds, '/%s/TriggerIn' % det, ohprovider)

        # Publish per partition TriggerIn
        for det, part, ds in ti_part:
            publish_histos(ds, '/%s-%s/TriggerIn' % (det, part), ohprovider)

        # Checkout, unfold RobinNP data,
        # remove disabled channels and prepare dataset
        rm_ds = prepare_rm_dataset(readoutmodule, rm_attrs, ROLData)

        # global publication
        publish_histos(rm_ds, '/GLOBAL/ReadoutModule', ohprovider)

        for det in detectors:
            det_rm_ds = rm_ds.subset(detector_match(det))

            # per detector publication
            publish_histos(det_rm_ds, '/%s/ReadoutModule' % det,
                           ohprovider)

            for part in partitions[det]:
                part_rm_ds =\
                    det_rm_ds.subset(partition_match(part))
                
                # per partition publication
                publish_histos(part_rm_ds,
                               '/%s-%s/ReadoutModule' % (det, part),
                               ohprovider)

        stop = time.time()
        processtime = stop - start
        with args.wakeupcondition:
            if args.wakeupcondition.wait(period - (processtime % period)):
                break


def parseargs():

    parser = argparse.ArgumentParser(
        description='ROSMonitoring reborn',
        epilog='and that\'s how you should use the programm',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-p', '--partition', help=' ',
                        default=os.getenv('TDAQ_PARTITION', None))
    parser.add_argument('-i', '--is-server', help=' ', default='DF')
    parser.add_argument('-o', '--oh-server', help=' ', default='Histogramming')
    parser.add_argument('-r', '--rc-server', help=' ', default='RunCtrl')
    script_name = os.path.splitext(os.path.basename(__file__))[0]
    parser.add_argument('-P', '--provider-name', help=' ', default=script_name)
    parser.add_argument('-t', '--period', type=float, help=' ', default=60.)

    return parser


def _main():

    parser = parseargs()
    args = parser.parse_args()

    if args.partition is None:
        sys.exit(parser.print_help())

    args.wakeupcondition = threading.Condition()
    handler = SignalHandler(args.wakeupcondition)
    args.stophandler = handler

    run(args)


if __name__ == '__main__':

    _main()
